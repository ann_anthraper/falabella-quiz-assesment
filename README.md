# Falabella Quiz Assessment

Prototype of a Quiz app for iOS.

**Requirements**
* Xcode 11.3
* iOS 13.2

**How to run the example?**
1. Clone this repo
2. Open shell window and navigate to project folder
3. Run pod install
4. Open FalabellaQuiz.xcworkspace and run the project on simulator.

**Features**
* The project is built using Apple's latest framework called SwiftUI.
* Fetch quiz questions from a JSON and store it in the database.
* Player gets 5 questions at a time.
* Each right answer awards 20 points and timer left on the next question.
* For each wrong answer 10 points will be deducted.
* At the end of the quiz session scorecard with best score and top 5 scores will be shown.
* Player can improve their scores by clicking on Play Again button.

**FLow chart**

![Image of Flowchart](Falabella_Flow_Chart.png)

**Screenshots**
![Image of Flowchart](splash.png)
![Image of Flowchart](onboarding_screen.png)
![Image of Flowchart](signup.png)
![Image of Flowchart](quiz.png)
![Image of Flowchart](leaderboard.png)

**Third Party Libraries**
1. Realm : used for saving user data and quiz questions 


