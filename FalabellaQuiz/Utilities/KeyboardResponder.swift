//
//  KeyboardResponder.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 20/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import SwiftUI
import Combine

final class KeyboardResponder: ObservableObject {
    
    private var notificationCenter: NotificationCenter
    
    @Published private(set) var currentHeight: CGFloat = 0
    
    var didChange = PassthroughSubject<Bool, Never>()
    var keyboardShown = Bool() {
        didSet {
            didChange.send(keyboardShown)
        }
    }
    
    init(center: NotificationCenter = .default) {
        notificationCenter = center
        notificationCenter.addObserver(self, selector: #selector(keyBoardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyBoardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    deinit {
        notificationCenter.removeObserver(self)
    }

    @objc func keyBoardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            currentHeight = keyboardSize.height
            keyboardShown = true
        }
    }

    @objc func keyBoardWillHide(notification: Notification) {
        currentHeight = 0
        keyboardShown = false
    }
}

