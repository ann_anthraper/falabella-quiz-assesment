//
//  PageControlView.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 20/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct PageControlView: UIViewRepresentable {
    
    var numberOfPages: Int
    
    @Binding var currentPageIndex: Int
    
    func makeUIView(context: Context) -> UIPageControl {
        let control = UIPageControl()
        control.transform = CGAffineTransform(scaleX: 2, y: 2)
        control.numberOfPages = numberOfPages
        control.currentPageIndicatorTintColor = UIColor.white
        control.pageIndicatorTintColor = UIColor.gray

        return control
    }
    
    func updateUIView(_ uiView: UIPageControl, context: Context) {
        uiView.currentPage = currentPageIndex
    }
    
}
