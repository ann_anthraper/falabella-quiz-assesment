//
//  SignupView.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 20/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI


struct SignupView: View {
    
    @ObservedObject private var keyboard = KeyboardResponder()
    
    @State var username: String = ""
    @State var age: String = ""
    
    @State var alertText: String = ""
    @State var showingAlert = false
    
    @State var signInSuccess: Bool = false
    
    static let gender = ["Male", "Female"]
    @State private var index = 0

    init() {
        UISegmentedControl.appearance().selectedSegmentTintColor = ApplicationConstants.signupThemeUIColor
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: ApplicationConstants.signupThemeUIColor], for: .normal)
    }
    
    var body: some View {
        GeometryReader { metrics in
            ZStack {
                Image("bg_signup")
                    .resizable()
                    .frame(width: metrics.size.width, height: metrics.size.height)
                    .aspectRatio(contentMode: .fill)
                    .clipped()
                
                VStack {
                    VStack {
                        Text("Welcome,")
                            .padding()
                            .font(.custom("Palatino-Bold", size: 30))
                            .foregroundColor(Color(hex: ApplicationConstants.quizThemeColor))
                            .frame(width: metrics.size.width, height: 50, alignment: .leading)
                        
                        Text("Sign in to continue!")
                            .padding()
                            .font(.custom("Palatino-Bold", size: 25))
                            .foregroundColor(.gray)
                            .frame(width: metrics.size.width, height: 50, alignment: .leading)
                            .offset(y: -20)
                        
                    }
                    .frame(width: metrics.size.width * 0.90, height: metrics.size.height * 0.30)
                    .padding(EdgeInsets(top: 50, leading: 0, bottom: 0, trailing: 0))
                    
                    VStack {
                        TextField("Full Name", text: self.$username)
                            .padding()
                            .textContentType(.username)
                            .keyboardType(.default)
                            .autocapitalization(.words)
                            .disableAutocorrection(true)
                            .font(Font.system(size: 18, design: .default))
                            .background(Color(hex: ApplicationConstants.gray100))
                            .opacity(0.8)
                            .cornerRadius(4.0)
                        
                        TextField("Age", text: self.$age)
                            .padding()
                            .textContentType(.none)
                            .keyboardType(.numberPad)
                            .autocapitalization(.none)
                            .disableAutocorrection(true)
                            .font(Font.system(size: 18, design: .default))
                            .background(Color(hex: ApplicationConstants.gray100))
                            .opacity(0.8)
                            .cornerRadius(4.0)
                        
                        HStack {
                            Text("Gender")
                                .font(Font.system(size: 18, design: .default))
                                .foregroundColor(Color.secondary)
                                .padding()
                            
                            Picker("Gender", selection: self.$index) {
                                ForEach(0 ..< Self.gender.count) {
                                    Text("\(Self.gender[$0])")
                                }
                            }.pickerStyle(SegmentedPickerStyle())
                                .padding()
                        }
                        .frame(height: 50)
                        .background(Color(hex: ApplicationConstants.gray100))
                        .opacity(0.8)
                        .cornerRadius(4.0)
                        
                        Spacer()
                        
                        Button(action: self.onSignUp) {
                            HStack(alignment: .center) {
                                Spacer()
                                Text("Sign in")
                                    .padding()
                                    .font(.custom("Palatino-Bold", size: 20))
                                    .foregroundColor(.white)
                                Spacer()
                                
                            }
                            .background(LinearGradient(gradient: Gradient(colors: [Color(hex: "#51293A"), Color(hex: "#036798") ]), startPoint: .leading, endPoint: .trailing))
                            .cornerRadius(15.0)
                            .padding(EdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 20))
                        }
                        .alert(isPresented: self.$showingAlert) {
                            Alert(title: Text("Error"), message: Text(self.alertText), dismissButton: .default(Text("OK")))
                            
                        }
                    }
                    .frame(width: metrics.size.width * 0.90, height: metrics.size.height * 0.40)
                    
                    Spacer()
                }
                .frame(width: metrics.size.width, height: metrics.size.height)
                
            }
            .frame(width: metrics.size.width, height: metrics.size.height)
            
            if self.signInSuccess {
                NavigationLink(destination: QuizView(), isActive: self.$signInSuccess) {
                    EmptyView()
                }
            }
        }
        .onDisappear() {
            self.username = ""
            self.age = ""
            self.index = 0
        }
        .padding(.bottom, self.keyboard.currentHeight)
        .animation(.easeOut(duration: 0.5))
        .navigationBarHidden(true)
        .navigationBarTitle(Text(""))
        .edgesIgnoringSafeArea([.top, .bottom])
    }
    
    private func onSignUp() {
        guard !(username.isEmpty || age.isEmpty || SignupView.gender[index].isEmpty) else {
            alertText = "All fields are mandatory!"
            showingAlert = true
            return
        }

        let avatar = NSData(data: AvatarGenerator.imageWith(name: self.username)!.pngData()!)
        try! RealmConstants.realm.write {
            let userModel = UserInfoModel(value: [self.username, Int(self.age)!, SignupView.gender[index], avatar, []])
            RealmConstants.realm.add(userModel)
            signInSuccess = true
        }
    }
}

struct SignupView_Previews: PreviewProvider {
    static var previews: some View {
        SignupView()
    }
}
