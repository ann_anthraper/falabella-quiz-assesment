//
//  QuizModel.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 21/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import RealmSwift

class QuizModel: Object, Codable {
    
    @objc dynamic var question: String = ""
    dynamic var options = List<String>()
    @objc dynamic var answer: String = ""
}
