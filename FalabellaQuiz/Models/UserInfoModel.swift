//
//  UserInfoModel.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 21/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import RealmSwift
import UIKit

class UserInfoModel: Object {
    
    @objc dynamic var name = ""
    @objc dynamic var age = 0
    @objc dynamic var gender = ""
    @objc dynamic var avatar : NSData? = nil
    dynamic var score = List<Int>()

}
