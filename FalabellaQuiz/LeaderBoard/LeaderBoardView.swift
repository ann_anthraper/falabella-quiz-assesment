//
//  LeaderBoardView.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 21/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct LeaderBoardView: View {
    
    @EnvironmentObject var model: PresentationModel
    
    var userInfo = RealmConstants.realm.objects(UserInfoModel.self)
    
    @State var image = UIImage()
    @State var bestScore = 0
    @State var topScores: [Int] = []
    
    var body: some View {
        GeometryReader { metrics in
            ZStack {
                Image("bg_leaderboard")
                    .resizable()
                    .frame(width: metrics.size.width, height: metrics.size.height)
                    .aspectRatio(contentMode: .fill)
                    .clipped()
                    .blur(radius: 3)
                
                VStack {
                    HStack {
                        Image(uiImage: self.image)
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 100, height: 100)
                            .clipShape(Circle())
                            .overlay(Circle().stroke(Color(hex: ApplicationConstants.leaderboardThemeColors.randomElement()!), lineWidth: 4))
                            .shadow(color: Color(hex: ApplicationConstants.leaderboardThemeColors.randomElement()!).opacity(0.7), radius: 5, x: 5, y: 5)
                            .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                        
                        HStack {
                            Text("Best Score : \(self.bestScore)")
                                .padding()
                                .font(.custom("Palatino-Bold", size: 25))
                                .multilineTextAlignment(.leading)
                                .foregroundColor(.white)
                        }
                        .frame(width: metrics.size.width * 0.60, height: 50)
                        .background(Color(hex: ApplicationConstants.leaderboardThemeColor).opacity(0.7))
                        .cornerRadius(10)
                        .offset(x:5)
                        
                        Spacer()
                    }
                    .frame(width: metrics.size.width, height: metrics.size.height * 0.25)
                    .padding(EdgeInsets(top: 0, leading: 75, bottom: 0, trailing: 20))
                    
                    ScrollView(.vertical, showsIndicators: false) {
                        VStack() {
                            ForEach(self.topScores, id: \.self) { item in
                                ScoreRowItemView(score: item, color: ApplicationConstants.leaderboardThemeColors.randomElement()!)
                                    .frame(width: metrics.size.width * 0.80, height: 60)
                            }
                        }
                    }
                    .frame(height:metrics.size.height * 0.50)
                    
                    
                    HStack {
                        
                        Button(action: {
                             exit(0)
                            
                        }) {
                            HStack(alignment: .center) {
                                Spacer()
                                Text("Exit")
                                    .padding()
                                    .font(.custom("Palatino", size: 20))
                                    .foregroundColor(.white)
                                Spacer()
                                
                            }
                            .background(LinearGradient(gradient: Gradient(colors: [Color(hex: "#57AB95"), Color(hex: "#E68E62"), Color(hex: "#C76C6E")]), startPoint: .leading, endPoint: .trailing))
                            .cornerRadius(15.0)
                            .padding(EdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 20))
                        }
                                                
                        Button(action: {
                            self.model.pushed = false
                            
                        }) {
                            HStack(alignment: .center) {
                                Spacer()
                                Text("Play Again")
                                    .padding()
                                    .font(.custom("Palatino", size: 20))
                                    .foregroundColor(.white)
                                Spacer()
                                
                            }
                            .background(LinearGradient(gradient: Gradient(colors: [Color(hex: "#57AB95"), Color(hex: "#E68E62"), Color(hex: "#C76C6E")]), startPoint: .leading, endPoint: .trailing))
                            .cornerRadius(15.0)
                            .padding(EdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 20))
                        }
                    }
                    .frame(width: metrics.size.width, height: 50)
                }
                
            }
        }
            
        .onAppear() {
            let data = self.userInfo.last!.avatar
            self.image = UIImage(data: data! as Data)!
            self.bestScore = Array(self.userInfo.last!.score).sorted().last!
            self.parseTopScores()
        }
        .navigationBarHidden(true)
        .navigationBarTitle(Text(""))
        .edgesIgnoringSafeArea([.top, .bottom])
    }
    
    private func parseTopScores() {
        let chunks = Array(self.userInfo.last!.score).sorted().removingDuplicates().splitBy(into: 5)
        self.topScores = chunks.last!.reversed()
    }
}

struct LeaderBoardView_Previews: PreviewProvider {
    static var previews: some View {
        LeaderBoardView()
    }
}
