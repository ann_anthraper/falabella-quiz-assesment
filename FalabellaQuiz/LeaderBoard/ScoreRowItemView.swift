//
//  ScoreRowItemView.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 21/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI
import UIKit

struct ScoreRowItemView: View {
    
    var score: Int
    var color: String
    
    var body: some View {
        
        GeometryReader { metrics in
            ZStack {
                ProgressBar(value: Double(7), maxValue: Double(10), backgroundColor: Color(hex: self.color).opacity(0.7), foregroundColor: Color(hex: self.color))
                
                HStack {
                    Image("leaderboard_user_icon")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .clipped()
                        .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 0))
                    
                    Spacer()
                    
                    
                    Text(String(self.score))
                        .foregroundColor(.white)
                        .font(.custom("AmericanTypewriter-Bold", size: 40))
                        .padding(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 50))
                    
                }
            }
            .frame(width: metrics.size.width, height: metrics.size.height)
            .cornerRadius(10)
            .shadow(color: Color(hex: self.color).opacity(0.7), radius: 5, x: 5, y: 5)

        }
    }
}


