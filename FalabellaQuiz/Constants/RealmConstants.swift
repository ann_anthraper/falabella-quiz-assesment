//
//  RealmConstants.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 21/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import RealmSwift

class RealmConstants {
    static let realm = try! Realm()
}

