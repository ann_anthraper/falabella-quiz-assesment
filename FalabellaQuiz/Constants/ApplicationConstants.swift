//
//  ApplicationConstants.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 20/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import Foundation
import UIKit

struct ApplicationConstants {
    
    static let onboardingThemeColor = "#80E2DB"
    
    static let gray100 = "#F5F5F5"
    static let quizThemeColor = "#51293A"
    static let quizOptionColors = ["#B84785", "#04A9C6", "#036798", "#6572A2", "#A1B7CE", "#F58402", "#016898"]
    static let leaderboardThemeColor = "#5FAE98"
    static let leaderboardThemeColors = ["#6D7A8A", "#8FC9AC", "#5DC4DA", "#E58D8E", "#FFC97E", "#298B9D", "#A385BE", "#278B9E"]
    
    static let signupThemeUIColor = UIColor(red: 81/256, green: 41/256, blue: 58/256, alpha: 1)
    
    static let quizTimer = 10
    
}
