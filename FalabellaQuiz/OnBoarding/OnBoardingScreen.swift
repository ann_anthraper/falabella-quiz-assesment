//
//  OnBoardingScreen.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 20/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct OnBoardingScreen: View {
    
    let totalPageCount = 3
    
    @State private var currentPage = 0
    
    var body: some View {
        GeometryReader { metrics in
            NavigationView {
                ZStack(alignment: .bottom) {
                    PaginationView(pageCount: self.totalPageCount, currentIndex: self.$currentPage) {
                        
                        VStack {
                            Spacer()
                            
                            Image("onboarding_brain_inside_bulb")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: metrics.size.width * 0.50, height: metrics.size.height * 0.50, alignment: .center)
                                .clipped()
                            
                            
                            Text("Highly engaged, entertaining and educational.")
                                .font(.custom("Palatino-Bold", size: 30))
                                .foregroundColor(.white)
                                .lineLimit(nil)
                                .multilineTextAlignment(.center)
                                .padding()
                                .offset(y : -50)
                            
                            Spacer()
                            
                        }
                        
                        VStack {
                            Spacer()
                            
                            Image("onboarding_timer")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: metrics.size.width * 0.50, height: metrics.size.height * 0.50, alignment: .center)
                                .clipped()
                            
                            Text("The quiz session test comprises 5 questions and each question will get 10sec to answer.")
                                .font(.custom("Palatino-Bold", size: 30))
                                .foregroundColor(.white)
                                .lineLimit(nil)
                                .multilineTextAlignment(.center)
                                .padding()
                                .offset(y : -50)
                            
                            Spacer()
                            
                        }
                        
                        VStack {
                            Spacer()
                            
                            Image("onboarding_prince_with_crown")
                                .resizable()
                                .aspectRatio(contentMode: .fit)
                                .frame(width: metrics.size.width * 0.50, height: metrics.size.height * 0.50, alignment: .center)
                                .clipped()
                            
                            Text("Practice and improve.")
                                .font(.custom("Palatino-Bold", size: 30))
                                .foregroundColor(.white)
                                .lineLimit(nil)
                                .multilineTextAlignment(.center)
                                .padding()
                                .offset(y : -50)
                            
                            
                            
                            NavigationLink(destination: SignupView()) {
                                HStack(alignment: .center) {
                                    Spacer()
                                    
                                    Text("Get Started")
                                        .font(.custom("Palatino-Bold", size: 25))
                                        .foregroundColor(Color(hex: ApplicationConstants.onboardingThemeColor))
                                        .frame(width: metrics.size.width * 0.50, height: 30, alignment: .center)
                                        .padding()
                                        .background(Color.white)
                                        .cornerRadius(30)
                                    
                                    Spacer()
                                    
                                }
                            }.buttonStyle(PlainButtonStyle())
                            
                            Spacer()
                        }
                    }
                    HStack {
                        Spacer()
                        
                        PageControlView(numberOfPages: self.totalPageCount, currentPageIndex: self.$currentPage)
                            .frame(height: 50)
                        
                        Spacer()
                    }
                    .padding(EdgeInsets(top: 20.0, leading: 20.0, bottom: 20.0, trailing: 20.0))
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
                .background(LinearGradient(gradient: Gradient(colors: [Color(hex: "#51293A"), Color(hex: "#09AD8A"), Color(hex: "#036798") ]), startPoint: .bottom, endPoint: .top))
                .edgesIgnoringSafeArea(.all)
            }
            .navigationViewStyle(StackNavigationViewStyle())
            .onAppear() {
                try! RealmConstants.realm.write {
                    RealmConstants.realm.deleteAll()
                }
            }
        }
    }
}


//MARK:- Preview
struct OnBoardingScreen_Previews: PreviewProvider {
    static var previews: some View {
        OnBoardingScreen()
    }
}
