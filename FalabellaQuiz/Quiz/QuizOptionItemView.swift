//
//  QuizOptionItemView.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 21/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct QuizOptionItemView: View {
    
    var title: String
    var body: some View {
        GeometryReader { metrics in
            HStack {
                Spacer()
                
                Text(self.title)
                    .font(.custom("Palatino", size: 20))
                    .fontWeight(.medium)
                    .foregroundColor(.white)
                
                Spacer()
                
            }
            .frame(width: metrics.size.width, height: metrics.size.height)
            .background(Color(hex: ApplicationConstants.quizOptionColors.shuffled().randomElement()!))
            .cornerRadius(10)
        }
    }
}
