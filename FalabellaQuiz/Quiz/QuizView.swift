//
//  QuizView.swift
//  FalabellaQuiz
//
//  Created by Ann Mary on 20/05/20.
//  Copyright © 2020 Ann Mary. All rights reserved.
//

import SwiftUI

struct QuizView: View {
    
    @EnvironmentObject var model: PresentationModel
    
    let maxQuestions = 5
    
    @State var timer = Timer.publish (every: 1, on: .main, in: .common).autoconnect()
    @State var counter = ApplicationConstants.quizTimer
    @State var score = 0
    @State var optionSelected = false
    
    @State var quizList = RealmConstants.realm.objects(QuizModel.self)
    
    @State var currentQuestionIndex = 1
    @State var question : String = ""
    @State var chunksQuizOptions : [[String]] = [[]]
    @State var answer : String = ""
        
    var body: some View {
        GeometryReader { metrics in
            VStack {
                VStack {
                    HStack(spacing : 0) {
                        Text("Question \(self.currentQuestionIndex)")
                            .font(.custom("Palatino-Bold", size: 30))
                            .foregroundColor(.white)
                            .lineLimit(nil)
                            .multilineTextAlignment(.leading)
                        
                        Text("/\(self.maxQuestions) ")
                            .font(.custom("Palatino-Bold", size: 25))
                            .foregroundColor(.secondary)
                        
                        Spacer()
                    }
                    .padding(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
                    
                    
                    ZStack {
                        ProgressBar(value: Double(self.counter), maxValue: Double(ApplicationConstants.quizTimer),backgroundColor: Color(hex: ApplicationConstants.quizThemeColor).opacity(0.7), foregroundColor: Color(hex: ApplicationConstants.quizThemeColor))
                            .frame(width: metrics.size.width, height: metrics.size.height * 0.03)
                        Text("\(self.counter) SECONDS")
                            .foregroundColor(.white)
                            .font(.custom("DIN Alternate", size: 18))
                    }
                    
                }
                .frame(width: metrics.size.width, height: metrics.size.height * 0.25)
                .padding(EdgeInsets(top: 20, leading: 20, bottom: 0, trailing: 20))
                
                HStack {
                    Text(self.question)
                        .padding()
                        .font(.custom("Palatino-Bold", size: 25))
                        .foregroundColor(.black)
                        .lineLimit(nil)
                        .multilineTextAlignment(.leading)
                        .padding(EdgeInsets(top: 20, leading: 0, bottom: 0, trailing: 0))
                    
                    Spacer()
                }
                .frame(width: metrics.size.width * 0.90, height: metrics.size.height * 0.30, alignment: .topLeading)
                .background(Color.white)
                .cornerRadius(20)
                .shadow(color: Color.secondary.opacity(0.7), radius: 5, x: 15, y: 15)
                .padding(EdgeInsets(top: 0, leading: 20, bottom: 30, trailing: 20))
                
                ForEach(self.chunksQuizOptions, id: \.self) { chunk in
                    VStack {
                        HStack {
                            ForEach(chunk, id: \.self) { item in
                                VStack {
                                    QuizOptionItemView(title: item)
                                        .frame(width: metrics.size.width * 0.40, height: 75)
                                }
                                .onTapGesture {
                                    self.optionSelected = true
                                    if self.answer == item {
                                        self.score += 20
                                    } else {
                                        self.score -= 10
                                    }
                                }
                            }
                            .padding(EdgeInsets(top: 5.0, leading: 5.0, bottom: 5.0, trailing: 5.0))
                        }
                    }
                }
                
                Spacer()
                
            }
            .frame(width: metrics.size.width, height: metrics.size.height)
            .background(LinearGradient(gradient: Gradient(colors: [Color(hex:"#0B6F62"), Color(hex: "#09AD8A"), Color(hex: "#7FE19E"), Color(hex: "#80E2DB")]), startPoint: .bottomLeading, endPoint: .topTrailing))
            
            NavigationLink(destination: LeaderBoardView(), isActive: self.$model.pushed) {
                EmptyView()
            }
        }
        .onAppear(perform: {
            self.parseQuizQuestions()
            self.instantiateTimer()
        }).onDisappear(perform: {
            self.cancelTimer()
        }).onReceive(timer) { _ in
            if self.optionSelected {
                self.currentQuestionIndex += 1
                self.getCurrentQuestion()
                self.counter = self.currentQuestionIndex == 1 ? ApplicationConstants.quizTimer : ApplicationConstants.quizTimer + self.counter
            } else {
                if self.counter > 0 {
                    self.counter -= 1
                } else {
                    self.counter = ApplicationConstants.quizTimer
                    self.currentQuestionIndex += 1
                    self.getCurrentQuestion()
                }
            }
        }
        .navigationBarHidden(true)
        .navigationBarTitle(Text(""))
        .edgesIgnoringSafeArea(.all)
        
    }
    
    func instantiateTimer() {
        self.timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
        return
    }
    
    func cancelTimer() {
        self.timer.upstream.connect().cancel()
        return
    }
    
    func parseQuizQuestions() {
        var arrQuizQuestions = Bundle.main.decode([QuizModel].self, from: "quiz.json")
        arrQuizQuestions = arrQuizQuestions.shuffled()
        try! RealmConstants.realm.write {
            let result = RealmConstants.realm.objects(QuizModel.self)
            RealmConstants.realm.delete(result)
        }
        for i in 0 ..< arrQuizQuestions.count {
            try! RealmConstants.realm.write {
                let quizModel = QuizModel(value: [arrQuizQuestions[i].question, arrQuizQuestions[i].options, arrQuizQuestions[i].answer])
                RealmConstants.realm.add(quizModel)
            }
        }
        quizList = RealmConstants.realm.objects(QuizModel.self)
        getCurrentQuestion()
    }
    
    private func getCurrentQuestion() {
        self.optionSelected = false
        if currentQuestionIndex <= maxQuestions {
            self.question = quizList[currentQuestionIndex].question
            self.chunksQuizOptions = Array(quizList[currentQuestionIndex].options).splitBy(into: 2)
            self.answer = quizList[currentQuestionIndex].answer
        } else {
            if let userModel = RealmConstants.realm.objects(UserInfoModel.self).last {
                try! RealmConstants.realm.write {
                    userModel.score.append(self.score)
                }
            }
            self.model.pushed = true
            self.currentQuestionIndex = 1
            self.score = 0
            self.counter = ApplicationConstants.quizTimer
        }
    }
}

struct QuizView_Previews: PreviewProvider {
    static var previews: some View {
        QuizView()
    }
}
